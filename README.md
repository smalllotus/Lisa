# Lisa v.4.3

## Max patcher for OSX and Windows 11, works with [BlackHole](https://existential.audio/blackhole/)

### Lisa is based upon Panoramix-Ircam.maxpat, and it uses various ressources from the [spat5](https://forum.ircam.fr/projects/detail/spat/) Max Package. 

The spat5 Max Package is developed by STMS Lab, UMR9912, Ircam / CNRS / Sorbonne Université (http://www.stms-lab.fr), and distributed by [Ircam Forum](https://forum.ircam.fr).

Lisa was developed with the kind technical support from the EAC team (https://www.stms-lab.fr/team/espaces-acoustiques-et-cognitifs/).

Lisa was originally a personal patch where I wanted to bring together the tools I wanted to use.
--------
**New feature**
- Added : Save Session button : Save the Panoramix Session script and set it as the default, it will be opened automatically when Lisa is launched.

**Updated with Panoramix 1.6.6 :** 

- Added : Dark mode design
- Added : Dropfile for loading Session script
- Added : Panoramix-Ircam.maxpat new features

-----------
- Added : Sound File Player
- Added : Record sound files (inputs & outputs)

### Panoramix (so Lisa) requires :

- [Spat 5.3.3 package](https://forum.ircam.fr/projects/detail/spat/)  

- [OSCar 1.2.5 plug-in](https://forum.ircam.fr/projects/detail/oscar/)

- [Max 8.6.4](https://cycling74.com/) 

For Help, Feedback, Professional use, and support, go to the [discussion group](https://discussion.forum.ircam.fr/c/lisa)
